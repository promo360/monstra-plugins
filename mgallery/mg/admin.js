function mgResize(siteurl) {
    var gm_result = $('#mg_result');
    gm_result.html('<img src="'+siteurl+'plugins/mgallery/mg/load.gif" width="16" height="16" alt=""/>');
    $.ajax({
        type: 'POST', 
        data: { gm_resize: true }
    }).done(function(data) {
        gm_result.html(data);
    });
    return false;
}

function mgSettingsSave(f) {
    var mg_result = $('#mg-settings-result');
    mg_result.html('<img src="'+f.siteurl.value+'plugins/mgallery/mg/load.gif" width="16" height="16" alt=""/>');
    $.ajax({
        type: 'POST', 
        data: $(f).serialize(),
        success: function(msg) {
            mg_result.html(msg);
        }
    });
    return false;
}