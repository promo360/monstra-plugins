<?php

    /**
     *  mGallery plugin
     *
     *  @package Monstra
     *  @subpackage Plugins
     *  @author Yudin Evgeniy / JINN
     *  @copyright 2012 Yudin Evgeniy / JINN
     *  @version 1.0.1
     *
     */


    // Register plugin
    Plugin::register( __FILE__,                    
                    __('mGallery', 'mgallery'),
                    __('mGallery plugin for Monstra', 'mgallery'),  
                    '1.0.1',
                    'JINN',                 
                    'http://monstra.promo360.ru/',
                    'mgallery');


    // Load mGallery Admin for Editor and Admin
    if (Session::exists('user_role') && in_array(Session::get('user_role'), array('admin', 'editor'))) {        
        Plugin::admin('mgallery');
    }
    
    if (!BACKEND) Stylesheet::add('plugins/mgallery/mg/style.css', 'frontend', 11);
    Shortcode::add('mgallery', 'mGallery::show');
    
    class mGallery extends Frontend {
        
        public static function title(){
            return __('mGallery', 'mgallery');
        }

        public static function content(){
            $html = '<h1>'.__('mGallery', 'mgallery').'</h1>';
            $html.= mGallery::show();
            return $html;
        }
        
        public static function show(){
            $mdir = ROOT . DS . 'public' . DS . 'mgallery' . DS;  
            $mthumbs   = $mdir . 'thumbs' . DS;
            $moriginal = $mdir . 'original' . DS;
            
            $width   = Option::get('mg_width');
            $height  = Option::get('mg_height');
            $siteurl = Option::get('siteurl');
            
            $pthumb = $siteurl.'public/mgallery/thumbs/';
            $poriginal = $siteurl.'public/mgallery/original/';
            
            $files = File::scan($mthumbs, 'jpg');
            $count = count($files);
            
            $html = '';
            $html.= '<div id="mgallery">';
            for($i=0;$i<$count;$i++) {
                $html.= '<a href="'.$poriginal.$files[$i].'"><img src="'.$pthumb.$files[$i].'" alt=""/></a>';
            }
            $html.= '</div>';
            return $html;
        }

        public static function template() {
            $template = Option::get('mg_template');
            return $template;
        }
    }
