<?php

    return array(
        'mgallery' => array(
            'mGallery' => 'Галерея',
            'Small gallery' => 'Маленькая галерея',
            'mGallery plugin for Monstra' => 'Плагин маленькой фотогалереи для Monstra',
            'mGallery template' => 'Шаблон маленькой галереи',
            'Save' => 'Сохранить',
            'Resize' => 'Обновить',
            'Settings' => 'Настройки',
            'Upload photo' => 'Загрузить фото',
            'Upload' => 'Загрузить',
            'Width thumbnails (px)' => 'Ширина миниатюры (px)', 
            'Height thumbnails (px)' => 'Высота миниатюры (px)', 
            'Resize way' => 'Способ уменьшения изображения',
            'Respect to the width' => 'Относительно ширины',
            'Respect to the height' => 'Относительно высоты',
            'Similarly, cutting unnecessary' => 'Точно, обрезая лишнее',
            'Similarly with the expansion' => 'Точно, растягивая изображение',
            'Original width (px, max)' => 'Ширина оригинала (px, max)',
            'Original height (px, max)' => 'Высота оригинала (px, max)',
            'sure' => 'Уверены, что хотите удалить эту фотографию',
            'Resize content' => 'Если по каким-либо причинам вы хотите изменить размер всех фотографий разом, то установите в настройках новый размер и нажмите на кнопку "Обновить".<br/>Наберитесь терпения, эта процедура может занять несколько минут.', 
            'Resize start' => 'Обновить миниатюры', 
            'Resize success!' => 'Обновление успешно завершено!', 
        )
    ); 