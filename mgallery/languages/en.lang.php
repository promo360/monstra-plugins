<?php

    return array(
        'mgallery' => array(
            'mGallery' => 'mGallery',
            'Small gallery' => 'Small gallery',
            'mGallery plugin for Monstra' => 'Small Gallery plugin for Monstra',
            'mGallery template' => 'mGallery template',
            'Save' => 'Save',
            'Resize' => 'Resize',
            'Settings' => 'Settings',
            'Upload photo' => 'Upload photo',
            'Upload' => 'Upload',
            'Width thumbnails (px)' => 'Width thumbnails (px)', 
            'Height thumbnails (px)' => 'Height thumbnails (px)', 
            'Resize way' => 'Resize way',
            'Respect to the width' => 'Respect to the width',
            'Respect to the height' => 'Respect to the height',
            'Similarly, cutting unnecessary' => 'Similarly, cutting unnecessary',
            'Similarly with the expansion' => 'Similarly with the expansion',
            'Original width (px, max)' => 'Original width (px, max)',
            'Original height (px, max)' => 'Original height (px, max)',
            'sure' => 'sure',
            'Resize content' => 'To change the size of all the photos, press the button and wait ...', 
            'Resize start' => 'Resize start', 
            'Resize success!' => 'Resize success!', 
        )
    ); 