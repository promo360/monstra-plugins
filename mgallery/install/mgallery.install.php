<?php defined('MONSTRA_ACCESS') or die('No direct script access.');

    Option::add('mg_width', 165);
    Option::add('mg_height', 100);
    Option::add('mg_wmax', 900);
    Option::add('mg_hmax', 800);
    Option::add('mg_resize', 'crop');
    Option::add('mg_template', 'index');     
    
    $mdir = ROOT . DS . 'public' . DS . 'mgallery' . DS;  
    $mthumbs   = $mdir . 'thumbs' . DS;
    $moriginal = $mdir . 'original' . DS;
   
    if(!is_dir($mdir))      mkdir($mdir, 0755);
    if(!is_dir($mthumbs))   mkdir($mthumbs, 0755);
    if(!is_dir($moriginal)) mkdir($moriginal, 0755);