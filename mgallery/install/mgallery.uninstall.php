<?php defined('MONSTRA_ACCESS') or die('No direct script access.');

    Option::delete('mg_width');
    Option::delete('mg_height');
    Option::delete('mg_wmax');
    Option::delete('mg_hmax');
    Option::delete('mg_resize');
    Option::delete('mg_template');