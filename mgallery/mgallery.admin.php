<?php 

    // Admin Navigation: add new item
    Navigation::add(__('mGallery', 'mgallery'), 'content', 'mgallery', 10);
    

    // Add actions
    Action::add('admin_pre_render','mGalleryAdmin::ajaxResize');
    Action::add('admin_pre_render','mGalleryAdmin::ajaxSaveSettings');
    Action::add('admin_themes_extra_index_template_actions','mGalleryAdmin::formComponent');
    Action::add('admin_themes_extra_actions','mGalleryAdmin::formComponentSave');
    
    Stylesheet::add('plugins/mgallery/mg/admin.css', 'backend', 11);
    Javascript::add('plugins/mgallery/mg/admin.js', 'backend', 11);

    /**
     * mGallery admin class
     */
    class mGalleryAdmin extends Backend {
        
	    public static function main() {    
            
            $resize_way = array(
                'width'   => __('Respect to the width', 'mgallery'),
                'height'  => __('Respect to the height', 'mgallery'),
                'crop'    => __('Similarly, cutting unnecessary', 'mgallery'),
                'stretch' => __('Similarly with the expansion', 'mgallery'), 
            );
            
            $mdir = ROOT . DS . 'public' . DS . 'mgallery' . DS;  
            $mthumbs   = $mdir . 'thumbs' . DS;
            $moriginal = $mdir . 'original' . DS;
            
            /**
             * Delete photo
             */
            if(Request::get('delete_img')) {
                if (Security::check(Request::get('token'))) {
                    unlink($mthumbs . Request::get('delete_img'));
                    unlink($moriginal . Request::get('delete_img'));
                    Request::redirect('index.php?id=mgallery');
                }
            }
            
            /**
             *  Upload image
             */   
            if (Request::post('upload_file')) {
                if (Security::check(Request::post('csrf'))) {      
                    if ($_FILES['file']) {
                        if($_FILES['file']['type'] == 'image/jpeg' ||
                            $_FILES['file']['type'] == 'image/png' ||
                            $_FILES['file']['type'] == 'image/gif') {
                   
                            $name = Text::random('alnum', 10).'.jpg';
                            $img  = Image::factory($_FILES['file']['tmp_name']);
                            
                            $wmax   = Option::get('mg_wmax');
                            $hmax   = Option::get('mg_hmax');
                            $width  = Option::get('mg_width');
                            $height = Option::get('mg_height');
                            
                            $ratio = $width/$height;
                            
                            if ($img->width > $wmax or $img->height > $hmax) {
                                if ($img->height > $img->width) {
                                    $img->resize($wmax, $hmax, Image::HEIGHT);
                                } else {
                                    $img->resize($wmax, $hmax, Image::WIDTH);
                                }
                            }
                            $img->save($moriginal . $name);
                            
                            switch (Option::get('mg_resize')) {
                                case 'width' :
                                    $img->resize($width, $height, Image::WIDTH);
                                    break;
                                case 'height' :
                                    $img->resize($width, $height, Image::HEIGHT);
                                    break;
                                case 'stretch' :
                                    $img->resize($width, $height);
                                    break;
                                default : 
                                    // crop
                                    if (($img->width/$img->height) > $ratio) {
                                        $img->resize($width, $height, Image::HEIGHT)->crop($width, $height, round(($img->width-$width)/2),0);
                                    } else {
                                        $img->resize($width, $height, Image::WIDTH)->crop($width, $height, 0, 0);
                                    }
                                    break;
                            }
                            $img->save($mthumbs . $name);
                        }
                    }
                    Request::redirect('index.php?id=mgallery');
                } else { die('csrf detected!'); }
            }
        
            Notification::setNow('upload', 'upload!');
            
            $files = File::scan($mthumbs, 'jpg');
            $siteurl = Option::get('siteurl');
            
            View::factory('mgallery/views/backend/index')
                ->assign('resize_way', $resize_way)
                ->assign('files', $files)
                ->assign('path_orig', $siteurl.'public/mgallery/original/')
                ->assign('path_thumb', $siteurl.'public/mgallery/thumbs/')
                ->display();
	    }
        
        /**
         *  Ajax save settings
         */ 
        public static function ajaxSaveSettings() {
            if (Request::post('mgallery_submit_settings')) {
                if (Security::check(Request::post('csrf'))) {
                    Option::update(array(
                        'mg_width'  => (int)Request::post('width_thumb'), 
                        'mg_height' => (int)Request::post('height_thumb'),
                        'mg_wmax'   => (int)Request::post('width_orig'),
                        'mg_hmax'   => (int)Request::post('height_orig'),
                        'mg_resize' => (string)Request::post('resize_way')
                    ));
                    exit('<b>'.__('Resize success!', 'mgallery').'</b>');
                } else { die('csrf detected!'); }
            }
        }
        
        /**
         *  Ajax Resize
         */ 
        public static function ajaxResize() {
            $mdir = ROOT . DS . 'public' . DS . 'mgallery' . DS;  
            $mthumbs   = $mdir . 'thumbs' . DS;
            $moriginal = $mdir . 'original' . DS;
            
            if(Request::post('gm_resize')) {
                $files = File::scan($mthumbs, 'jpg');
                if ($files > 0) {
                    $width  = Option::get('mg_width');
                    $height = Option::get('mg_height');
                    $resize_way = Option::get('mg_resize');
                    $ratio = $width/$height;
                       
                    foreach ($files as $name) {
                        $img = Image::factory($moriginal.$name);
                            
                        switch ($resize_way) {
                            case 'width' :
                                $img->resize($width, $height, Image::WIDTH);
                                break;
                            case 'height' :
                                $img->resize($width, $height, Image::HEIGHT);
                                break;
                            case 'stretch' :
                                $img->resize($width, $height);
                                break;
                            default : 
                                // crop                                    
                                if (($img->width/$img->height) > $ratio) {
                                    $img->resize($width, $height, Image::HEIGHT)->crop($width, $height, round(($img->width-$width)/2),0);
                                } else {
                                    $img->resize($width, $height, Image::WIDTH)->crop($width, $height, 0, 0);
                                }
                                break;
                        }
                        $img->save($mthumbs . $name);
                    }
                }
                exit('<b>'.__('Resize success!', 'mgallery').'</b>');
            }
        }

        /**
         * Form Component Save
         */
        public static function formComponentSave() {
            if (Request::post('mgallery_component_save')) {
                if (Security::check(Request::post('csrf'))) {
                    Option::update('mg_template', Request::post('mgallery_form_template'));
                    Request::redirect('index.php?id=themes');
                }
            }
        }


        /**
         * Form Component
         */
        public static function formComponent() {

            $_templates = Themes::getTemplates();

            foreach($_templates as $template) {
                $t = basename($template, '.template.php');
                $templates[$t] = $t;
            }
           
            echo (
                Form::open().
                Form::hidden('csrf', Security::token()).
                Form::label('mgallery_form_template', __('mGallery template', 'mgallery')).
                Form::select('mgallery_form_template', $templates, Option::get('mg_template')).
                Html::br().
                Form::submit('mgallery_component_save', __('Save', 'mgallery'), array('class' => 'btn')).        
                Form::close()
            );
        }


	}