# Simple gallery for Monstra CMS

Links: [http://monstra.promo360.ru/plugin/mini-gallery](http://monstra.promo360.ru/plugin/mini-gallery)

## Usage
Open the link [http://example.org/mgallery](http://example.org/mgallery) after installing the plugin

## Installation
* Download the latest release 
* Extract the content of the zip file into the Monstra 'plugins' directory

## History
* v.1.0.1 - css bug fixed and added ajax 
* v.1.0.0 - first public release

## Requirements
* Monstra CMS version 2.2.x or 2.3.x

Copyright (c) 2012-2014 Yudin Evgeniy / JINN [info@promo360.ru]