<h2><?php echo __('Small gallery', 'mgallery');?></h2>
<ul class="nav nav-tabs">
    <li <?php if (Notification::get('upload')) { ?>class="active"<?php } ?>><a href="#upload" data-toggle="tab"><?php echo __('Upload photo', 'mgallery'); ?></a></li>
    <li <?php if (Notification::get('settings')) { ?>class="active"<?php } ?>><a href="#settings" data-toggle="tab"><?php echo __('Settings', 'mgallery'); ?></a></li>
    <li <?php if (Notification::get('resize')) { ?>class="active"<?php } ?>><a href="#resize" data-toggle="tab"><?php echo __('Resize', 'mgallery'); ?></a></li>
</ul>
         
<div class="tab-content tab-page">
    <div class="tab-pane <?php if (Notification::get('upload')) { ?>active<?php } ?>" id="upload">
        <?php
        echo (
            Form::open(null, array('enctype' => 'multipart/form-data')).
            Form::hidden('csrf', Security::token()).
            Form::input('file', null, array('type' => 'file', 'size' => '25')).Html::br().
            Form::submit('upload_file', __('Upload', 'mgallery'), array('class' => 'btn default btn-small')).
            Form::close()
        );
        ?>        
    </div>
    <div class="tab-pane <?php if (Notification::get('settings')) { ?>active<?php } ?>" id="settings">
        <?php 
        
        echo (
            '<form onSubmit="return mgSettingsSave(this);" method="post">'.
            '<div style="float:left; margin-right:20px;">'.
            Form::label('width_thumb', __('Width thumbnails (px)', 'mgallery')).
            Form::input('width_thumb', Option::get('mg_width')).
            '</div><div style="float:left;">'.
            Form::label('height_thumb', __('Height thumbnails (px)', 'mgallery')).
            Form::input('height_thumb', Option::get('mg_height')).
            '</div><br clear="both"/>'.
            '<div style="float:left; margin-right:20px;">'.
            Form::label('width_orig', __('Original width (px, max)', 'mgallery')).
            Form::input('width_orig', Option::get('mg_wmax')).
            '</div><div style="float:left;">'.
            Form::label('height_orig', __('Original height (px, max)', 'mgallery')).
            Form::input('height_orig', Option::get('mg_hmax')).
            '</div><br clear="both"/>'.
            '<div style="float:left; margin-right:20px;">'.
            Form::label('resize_way', __('Resize way', 'mgallery')).
            Form::select('resize_way', $resize_way, Option::get('mg_resize')).Html::Br().
            '</div><div style="float:left; margin-top:2px;">'.
            Form::hidden('csrf', Security::token()).
            Form::hidden('siteurl', Option::get('siteurl')).
            Form::hidden('mgallery_submit_settings', true).
            Form::label('submit_settings', __('&nbsp;')).
            Form::submit('submit_settings', __('Save', 'mgallery'), array('class' => 'btn')).
            '</div><br clear="both"/><div id="mg-settings-result"></div>'.
            Form::close()
        );
        ?>  
    </div>
    <div class="tab-pane <?php if (Notification::get('resize')) { ?>active<?php } ?>" id="resize">
        <?php 
            echo __('Resize content', 'mgallery').Html::Br(2);
            echo Html::anchor(__('Resize start', 'mgallery'), '#',
           array('class' => 'btn btn-actions', 'onclick' => "return mgResize('".Option::get('siteurl')."');"));
        ?> 
        <div id="mg_result"></div>
    </div>
</div>
<br/>
<?php     
if(count($files)>0) {
    foreach($files as $row) {
        echo '<div class="mgallery_img">';
            echo '<a href="'.$path_orig.$row.'"><img src="'.$path_thumb.$row.'" alt=""/></a>';
            echo '<a href="index.php?id=mgallery&delete_img='.$row.'&token='.Security::token().'" onClick="return confirmDelete(\''.__('sure', 'mgallery').'\')" class="mg_hide mg_del">X</a>';
        echo '</div>';
    }
    echo '<br clear="both"/>';
}
?>