# Fancybox plugin for Monstra CMS

Use [Fancybox](http://fancyapps.com/fancybox/)

Links: [http://monstra.promo360.ru/plugin/fancybox](http://monstra.promo360.ru/plugin/fancybox)

## Installation
* Download the latest release 
* Extract the content of the zip file into the Monstra 'plugins' directory

## Modal
<a class="fmodal" href="#mydiv">Link</a>
<span id="mydiv" style="display:none">Text</span>

## History
* v.1.0.0 - first public release
* v.1.0.1 - added .fmodal

## Requirements
* Monstra CMS version 2.2.x or 2.3.x