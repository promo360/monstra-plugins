<?php

    /**
     *  Fancybox plugin
     *
     *  @package Monstra
     *  @subpackage Plugins
     *  @author Yudin Evgeniy / JINN
     *  @copyright 2013 Yudin Evgeniy / JINN
     *  @version 1.0.1
     *
     *  Fancybox
     *  http://fancyapps.com/fancybox/
     *  version: 2.1.4 (Thu, 17 Jan 2013)
     */


    // Register plugin
    Plugin::register( __FILE__,                    
                    __('Fancybox', 'fancybox'),
                    __('Fancybox plugin for Monstra', 'fancybox'),  
                    '1.0.1',
                    'JINN',                 
                    'http://monstra.promo360.ru/');
    
    // Frontend
    Action::add('theme_header', 'FancyboxCSS');
    Javascript::add('plugins/fancybox/lib/source/jquery.fancybox.pack.js', 'frontend', 13);
    Javascript::add('plugins/fancybox/lib/settings.js', 'frontend', 13);
    
    // Backend
    Action::add('admin_header', 'FancyboxCSS');
    Javascript::add('plugins/fancybox/lib/source/jquery.fancybox.pack.js', 'backend', 13);
    Javascript::add('plugins/fancybox/lib/settings.js', 'backend', 13);
    
    function FancyboxCSS() {
        echo '<link rel="stylesheet" href="'.Site::url().'plugins/fancybox/lib/source/jquery.fancybox.css" type="text/css" />';
    }