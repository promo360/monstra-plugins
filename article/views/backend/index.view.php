<div class="row-fluid">
    <div class="span12">
        <h2><?php echo __('Article', 'article'); ?></h2><br />

        <?php 
        if (Notification::get('success')) Alert::success(Notification::get('success'));
        echo (Html::anchor(__('Add article', 'article'), 'index.php?id=article&action=add', array('class' => 'btn default btn-small'))).Html::Nbsp(2);
        echo (Html::anchor(__('Settings', 'article'), 'index.php?id=article&action=settings', array('class' => 'btn default btn-small')));
        ?>
        <br /><br />
        
        
        <ul class="article-sort">
            <li><b><?php echo __('Sort by:', 'article');?></b> &nbsp;</li>
            
            <li><a href="index.php?id=article&page=<?php echo $current_page;?>&sort=date&order=<?php echo $order;?>"<?php if($sort=='date') echo ' class="current"';?>><?php echo __('by date', 'article');?></a> <div class="article-line">/</div></li>
            <li><a href="index.php?id=article&page=<?php echo $current_page;?>&sort=id&order=<?php echo $order;?>"<?php if($sort=='id') echo ' class="current"';?>><?php echo __('by number', 'article');?></a> <div class="article-line">/</div></li>
            <li><a href="index.php?id=article&page=<?php echo $current_page;?>&sort=hits&order=<?php echo $order;?>"<?php if($sort=='hits') echo ' class="current"';?>><?php echo __('by views', 'article');?></a> <div class="article-line">/</div></li>
            <li><a href="index.php?id=article&page=<?php echo $current_page;?>&sort=status&order=<?php echo $order;?>"<?php if($sort=='status') echo ' class="current"';?>><?php echo __('by status', 'article');?></a></li>
            
            <li class="article-right">
                <a href="index.php?id=article&page=<?php echo $current_page;?>&sort=<?php echo $sort;?>&order=ASC"<?php if($order=='ASC') echo ' class="current"';?>><?php echo __('ASC', 'article');?></a>
            </li>
            <li class="article-right">
                <a href="index.php?id=article&page=<?php echo $current_page;?>&sort=<?php echo $sort;?>&order=DESC"<?php if($order=='DESC') echo ' class="current"';?>><?php echo __('DESC', 'article');?></a> 
                <div class="article-line">/</div>
            </li>
        </ul>

        
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th><?php echo __('Name', 'article'); ?></th>
                    <th><?php echo __('Author', 'article'); ?></th>
                    <th><?php echo __('Status', 'article'); ?></th>
                    <th><?php echo __('Hits', 'article'); ?></th>
                    <th><?php echo __('Date', 'article'); ?></th>
                    <th width="40%"><?php echo __('Actions', 'article'); ?></th>
                </tr>
            </thead>
            <tbody>
            <?php if (count($article_list) != 0): ?> 
            <?php foreach ($article_list as $row): ?>
             <tr>        
                <td><?php echo Html::anchor(Html::toText($row['name']), $site_url.'article/'.$row['id'].'/'.$row['slug'], 
                    array('target' => '_blank')); ?></td>
                <td><?php echo $row['author']; ?></td>
                <td><?php echo $status_array[$row['status']]; ?></td>
                <td><?php echo $row['hits']; ?></td>
                <td><?php echo Date::format($row['date'], "j.n.Y"); ?></td>
                <td>
                    <div class="btn-toolbar">
                        <div class="btn-group">
                            <?php 
                            echo (
                                Html::anchor(__('Edit', 'article'), 'index.php?id=article&action=edit&article_id='.$row['id'], 
                                    array('class' => 'btn btn-actions btn-actions-default')).Html::Nbsp(2).
                            
                                Html::anchor(__('Delete', 'article'), 'index.php?id=article&action=delete&article_id='.$row['id'].'&token='.Security::token(),
                                    array('class' => 'btn btn-actions', 
                                        'onclick' => "return confirmDelete('".__("Delete article: :article", 'article', 
                                            array(':article' => Html::toText($row['name'])))."')"))
                            );
                            ?>
                        </div>
                    </div>  
                </td>
             </tr> 
            <?php
            endforeach;
            endif;
            ?>
            </tbody>
        </table>
        <div id="article-paginator-admin"><?php Article::paginator($current_page, $pages_count, 'index.php?id=article&sort='.$sort.'&order='.$order.'&page=');?></div>
    </div>
</div>