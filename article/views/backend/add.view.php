<div class="row-fluid">
    <div class="span12">
        <h2><?php echo __('New article', 'article'); ?></h2><br />

        <?php
            if (Notification::get('success')) Alert::success(Notification::get('success'));
            
            echo Form::open(null, array('enctype' => 'multipart/form-data'));
            echo Form::hidden('csrf', Security::token());
        ?>

        <ul class="nav nav-tabs">
            <li <?php if (Notification::get('article')) { ?>class="active"<?php } ?>><a href="#article" data-toggle="tab"><?php echo __('Caption', 'article'); ?></a></li>
            <li <?php if (Notification::get('seo')) { ?>class="active"<?php } ?>><a href="#seo" data-toggle="tab"><?php echo __('SEO', 'article'); ?></a></li>
            <li <?php if (Notification::get('settings')) { ?>class="active"<?php } ?>><a href="#settings" data-toggle="tab"><?php echo __('Settings', 'article'); ?></a></li>
        </ul>
         
        <div class="tab-content tab-page">
            <div class="tab-pane <?php if (Notification::get('article')) { ?>active<?php } ?>" id="article">
                <?php
                    echo Form::input('article_name', $post_name, array('class' => (isset($errors['article_empty_name'])) ? 'span8 error-field' : 'span8'));
                    if (isset($errors['article_empty_name'])) echo Html::nbsp(3).'<span style="color:red">'.$errors['article_empty_name'].'</span>';
                ?>
            </div>
            <div class="tab-pane <?php if (Notification::get('seo')) { ?>active<?php } ?>" id="seo">
                <?php
                    echo (
                        Form::label('article_title', __('Title', 'article')).    
                        Form::input('article_title', $post_title, array('class' => 'span8')).

                        Form::label('article_h1', __('H1', 'article')).    
                        Form::input('article_h1', $post_h1, array('class' => 'span8')).
                        
                        Form::label('article_slug', __('Alias (slug)', 'article')).    
                        Form::input('article_slug', $post_slug, array('class' => 'span8')).

                        Form::label('article_keywords', __('Keywords', 'article')).
                        Form::input('article_keywords', $post_keywords, array('class' => 'span8')).

                        Form::label('article_description', __('Description', 'article')).
                        Form::textarea('article_description', $post_description, array('class' => 'span8'))
                    );
                ?>
            </div>
            <div class="tab-pane <?php if (Notification::get('settings')) { ?>active<?php } ?>" id="settings">
                <div class="row-fluid">
                    <div class="span4">
                    <?php 
                        echo (
                            Form::label('status', __('Status', 'article')).
                            Form::select('status', $status_array, 'published') 
                        );
                    ?>
                    </div>
                    <div class="span4">
                    <?php 
                        echo (
                            Form::label('file', __('Image', 'article')).
                            Form::input('file', null, array('type' => 'file', 'size' => '25'))
                        );
                    ?>
                    </div>
                </div>
            </div>
        </div>
    
        <br /><br />
        <?php Action::run('admin_editor', array(Html::toText($post_content))); ?>
        <br />

        <div class="row-fluid">
            <div class="span6">
                <?php
                    echo (
                        Form::submit('add_article_and_exit', __('Save and exit', 'article'), array('class' => 'btn')).Html::nbsp(2).
                        Form::submit('add_article', __('Save', 'article'), array('class' => 'btn'))
                    );
                ?>
            </div>
            <div class="span6">
                <div class="pull-right"><?php echo __('Published', 'article'); ?>: <?php echo Form::input('article_date', $date, array('class' => 'input-large')); ?></div>
                <?php echo Form::close(); ?>
            </div>
        </div>
    </div>
</div>