# Article plugin for Monstra CMS

Links: [http://monstra.promo360.ru/plugin/article-plugin](http://monstra.promo360.ru/plugin/article-plugin)

## Usage
Open the link [http://example.org/article](http://example.org/article) after installing the plugin

## Installation
* Download the latest release 
* Extract the content of the zip file into the Monstra 'plugins' directory

## History
* v.1.0.1 - first public release

## Requirements
* Monstra CMS version 2.2.x or 2.3.x

Copyright (c) 2012-2014 Yudin Evgeniy / JINN [info@promo360.ru]