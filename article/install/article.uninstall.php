<?php defined('MONSTRA_ACCESS') or die('No direct script access.');

    // Delete Options
    Option::delete('article_template');
    Option::delete('article_limit');
    Option::delete('article_limit_admin');
    Option::delete('article_w');
    
    Table::drop('article');
    
    function articleRemoveDir($dir) {
        if ($objs = glob($dir."/*")) {
            foreach($objs as $obj) {
                is_dir($obj) ? articleRemoveDir($obj) : unlink($obj);
            }
        }
        rmdir($dir);
    }
    
    articleRemoveDir(ROOT . DS . 'storage' . DS . 'article');
    articleRemoveDir(ROOT . DS . 'public'  . DS . 'article');
    