# Guestbook plugin for Monstra CMS

Links: [http://monstra.promo360.ru/plugin/guestbook](http://monstra.promo360.ru/plugin/guestbook)

## Usage
Open the link [http://example.org/guestbook](http://example.org/guestbook) after installing the plugin

## Installation
* Download the latest release 
* Extract the content of the zip file into the Monstra 'plugins' directory

## History
* v.1.0.0 - first public release

## Requirements
* Monstra CMS version 2.2.x or 2.3.x

Copyright (c) 2013-2014 Yudin Evgeniy / JINN [info@promo360.ru]