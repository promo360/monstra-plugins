<?php if (Notification::get('success')) Alert::success(Notification::get('success')); ?>
<?php if (Notification::get('error')) Alert::error(Notification::get('error')); ?>

<h2><?php echo __('Guestbook', 'guestbook');?></h2><br/>

<div class="btn-group">
    <a class="btn btn-small" href="index.php?id=guestbook&action=settings"><?php echo __('Settings', 'guestbook');?></a> 
    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown">
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
        <li><a href="#guestbookCodeModal" role="button" data-toggle="modal"><?php echo __('View Embed Code', 'guestbook');?></a></li>
    </ul>
</div>
<br clear="both"/><br/>

<form method="post" onSubmit="">
<table class="table table-bordered">
    <thead>
        <tr>
            <th width="20">&nbsp;</th>
            <th width="80"><?php echo __('Date', 'guestbook');?></th>
            <th width="130"><?php echo __('Name', 'guestbook');?></th>
            <th><?php echo __('Message', 'guestbook'); ?></th>
            <?php if (Option::get('guestbook_check') == 'yes') { ?>
                <th>&nbsp;</th>
            <?php } ?>
            <th width="50"><?php echo __('Important', 'guestbook');?></th>
            <th width="185"><?php echo __('Actions', 'guestbook'); ?></th>
        </tr>
    </thead>
    <?php if (count($records) > 0): ?>
    <tbody>
        <?php foreach ($records as $row):?>
        <tr <?php if (Option::get('guestbook_check') == 'yes' and $row['check'] == 0) echo 'class="warning"'; ?>>
            <td><input type="checkbox" name="guestbook_delete[]" class="guestbook-delete" value="<?php echo $row['id'];?>"/></td>
            <td><?php echo Guestbook::getdate($row['date']); ?></td>
            <td><?php echo $row['name']; ?></td>
            <td>
                <?php echo $row['message'];?>
                <?php if ($row['answer'] != '') echo '<blockquote style="margin-bottom:0;"><small>'.$row['answer'].'</small></blockquote>';?>
            </td>
            <?php if (Option::get('guestbook_check') == 'yes') { ?>
                <td>
                <?php if ($row['check'] == 0) { ?>
                    <a class="btn btn-small btn-danger guestbook-check" href="#" data-id="<?php echo $row['id'];?>" title="<?php echo __('Confirmed', 'guestbook');?>"><i class="icon-thumbs-up icon-white"></i></a>
                <?php } ?>
                </td>
            <?php } ?>
            <td><a class="btn btn-small <?php if ($row['important']) echo 'btn-info';?> guestbook-impotant-check" href="#" data-id="<?php echo $row['id'];?>"><i class="icon-ok <?php if ($row['important']) echo 'icon-white';?>"></i></a></td>
            <td>
                <?php echo Html::anchor(__('Edit', 'guestbook'), 'index.php?id=guestbook&row_id='.$row['id'].'&action=edit', array('class' => 'btn btn-small')); ?>
                <?php echo Html::anchor(__('Delete', 'guestbook'), 'index.php?id=guestbook&row_id='.$row['id'].'&action=delete&page='.Request::get('page').'&token='.Security::token(), array('class' => 'btn btn-small', 'onClick'=>'return confirmDelete(\''.__('Sure you want to remove the :item', 'guestbook', array(':item'=>$row['name'])).'\')')); ?>
            </td>
        </tr>
        <?php endforeach;?>
    </tbody> 
    <?php endif; ?>
</table>
<?php echo Form::hidden('csrf', Security::token());?>
<input type="checkbox" class="check-all" style="margin-left:16px; margin-right:13px;"/>
<input type="submit" name="submit_delete_guestbook" class="btn delete-guestbook-button disabled" disabled="disabled" value="<?php echo __('Delete checked', 'guestbook');?>" onClick="return confirmDelete('<?php echo __('Sure you want to delete all the selected records', 'guestbook');?>');"/>
</form>

<div class="guestbook-paginator"><?php Guestbook::paginator($current_page, $pages_count, 'index.php?id=guestbook&page=');?></div>

<!-- Modal -->
<div id="guestbookCodeModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="guestbookModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><?php echo __('Embed Code', 'guestbook');?></h3>
    </div>
    <div class="modal-body">
        <?php echo __('PHP Code', 'guestbook');?><br/>
        
        <dl class="dl-horizontal">
            <dt><?php echo __('Count guestbook', 'guestbook');?></dt>
            <dd><code>&lt;?php echo Guestbook::count();?&gt;</code></dd>

            <dt><?php echo __('Random 3', 'guestbook');?></dt>
            <dd><code>&lt;?php Guestbook::show('random', 3);?&gt;</code></dd>
            
            <dt><?php echo __('Last 3 important', 'guestbook');?></dt>
            <dd><code>&lt;?php Guestbook::show('last', 3, 'important');?&gt;</code></dd>
        </dl>
    
        <?php echo __('Shortcode', 'guestbook');?><br/>
        
        <dl class="dl-horizontal">
            <dt><?php echo __('Count guestbook', 'guestbook');?></dt>
            <dd><code>{guestbook show="count"}</code></dd>

            <dt><?php echo __('Random 3', 'guestbook');?></dt>
            <dd><code>{guestbook show="random" count="3"}</code></dd>
            
            <dt><?php echo __('Last 3 important', 'guestbook');?></dt>
            <dd><code>{guestbook show="last" count="3" label="important"}</code></dd>
        </dl>
    </div>
</div>