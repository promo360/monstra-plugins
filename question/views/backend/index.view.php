<?php if (Notification::get('success')) Alert::success(Notification::get('success')); ?>
<?php if (Notification::get('error')) Alert::error(Notification::get('error')); ?>

<h2><?php echo __('Question', 'question');?></h2><br/>

<div class="btn-group">
    <a class="btn btn-small" href="index.php?id=question&action=settings"><?php echo __('Settings', 'question');?></a> 
    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown">
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
        <li><a href="#questionCodeModal" role="button" data-toggle="modal"><?php echo __('View Embed Code', 'question');?></a></li>
    </ul>
</div>
<br clear="both"/><br/>

<form method="post" onSubmit="">
<table class="table table-bordered">
    <thead>
        <tr>
            <th width="20">&nbsp;</th>
            <th width="80"><?php echo __('Date', 'question');?></th>
            <th width="130"><?php echo __('Name', 'question');?></th>
            <th><?php echo __('Message', 'question'); ?></th>
            <?php if (Option::get('question_check') == 'yes') { ?>
                <th>&nbsp;</th>
            <?php } ?>
            <th width="50"><?php echo __('Important', 'question');?></th>
            <th width="185"><?php echo __('Actions', 'question'); ?></th>
        </tr>
    </thead>
    <?php if (count($records) > 0): ?>
    <tbody>
        <?php foreach ($records as $row):?>
        <tr <?php if (Option::get('question_check') == 'yes' and $row['check'] == 0) echo 'class="warning"'; ?>>
            <td><input type="checkbox" name="question_delete[]" class="question-delete" value="<?php echo $row['id'];?>"/></td>
            <td><?php echo Question::getdate($row['date']); ?></td>
            <td><?php echo $row['name']; ?></td>
            <td>
                <?php echo $row['message'];?>
                <?php if ($row['answer'] != '') echo '<blockquote style="margin-bottom:0;"><small>'.$row['answer'].'</small></blockquote>';?>
            </td>
            <?php if (Option::get('question_check') == 'yes') { ?>
                <td>
                <?php if ($row['check'] == 0) { ?>
                    <a class="btn btn-small btn-danger question-check" href="#" data-id="<?php echo $row['id'];?>" title="<?php echo __('Confirmed', 'question');?>"><i class="icon-thumbs-up icon-white"></i></a>
                <?php } ?>
                </td>
            <?php } ?>
            <td><a class="btn btn-small <?php if ($row['important']) echo 'btn-info';?> question-impotant-check" href="#" data-id="<?php echo $row['id'];?>"><i class="icon-ok <?php if ($row['important']) echo 'icon-white';?>"></i></a></td>
            <td>
                <?php echo Html::anchor(__('Edit', 'question'), 'index.php?id=question&row_id='.$row['id'].'&action=edit', array('class' => 'btn btn-small')); ?>
                <?php echo Html::anchor(__('Delete', 'question'), 'index.php?id=question&row_id='.$row['id'].'&action=delete&page='.Request::get('page').'&token='.Security::token(), array('class' => 'btn btn-small', 'onClick'=>'return confirmDelete(\''.__('Sure you want to remove the :item', 'question', array(':item'=>$row['name'])).'\')')); ?>
            </td>
        </tr>
        <?php endforeach;?>
    </tbody> 
    <?php endif; ?>
</table>
<?php echo Form::hidden('csrf', Security::token());?>
<input type="checkbox" class="check-all" style="margin-left:16px; margin-right:13px;"/>
<input type="submit" name="submit_delete_question" class="btn delete-question-button disabled" disabled="disabled" value="<?php echo __('Delete checked', 'question');?>" onClick="return confirmDelete('<?php echo __('Sure you want to delete all the selected records', 'question');?>');"/>
</form>

<div class="question-paginator"><?php Question::paginator($current_page, $pages_count, 'index.php?id=question&page=');?></div>

<!-- Modal -->
<div id="questionCodeModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="questionModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><?php echo __('Embed Code', 'question');?></h3>
    </div>
    <div class="modal-body">
        <?php echo __('PHP Code', 'question');?><br/>
        
        <dl class="dl-horizontal">
            <dt><?php echo __('Count question', 'question');?></dt>
            <dd><code>&lt;?php echo Question::count();?&gt;</code></dd>

            <dt><?php echo __('Random 3', 'question');?></dt>
            <dd><code>&lt;?php Question::show('random', 3);?&gt;</code></dd>
            
            <dt><?php echo __('Last 3 important', 'question');?></dt>
            <dd><code>&lt;?php Question::show('last', 3, 'important');?&gt;</code></dd>
        </dl>
    
        <?php echo __('Shortcode', 'question');?><br/>
        
        <dl class="dl-horizontal">
            <dt><?php echo __('Count question', 'question');?></dt>
            <dd><code>{question show="count"}</code></dd>

            <dt><?php echo __('Random 3', 'question');?></dt>
            <dd><code>{question show="random" count="3"}</code></dd>
            
            <dt><?php echo __('Last 3 important', 'question');?></dt>
            <dd><code>{question show="last" count="3" label="important"}</code></dd>
        </dl>
    </div>
</div>