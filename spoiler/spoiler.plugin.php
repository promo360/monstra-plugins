<?php

    /**
     *  Spoiler plugin 
     *
     *  @package Monstra
     *  @subpackage Plugins
     *  @author Yudin Evgeniy / JINN
     *  @copyright 2012-2013 Yudin Evgeniy / JINN
     *  @version 1.0.4
     *
     */


    // Register plugin
    Plugin::register( __FILE__,                    
                    __('Spoiler', 'spoiler'),
                    __('Spoiler plugin for Monstra', 'spoiler'),  
                    '1.0.4',
                    'JINN',
                    'http://monstra.promo360.ru/');

    Shortcode::add('spoiler', 'Spoiler::_shortcode');

    Javascript::add('plugins/spoiler/lib/script.js', 'frontend', 11);
    Stylesheet::add('plugins/spoiler/lib/style.css', 'frontend', 11);
    
    Action::add('admin_header','Spoiler::_header', 9);
    
    class Spoiler {
        public static function _shortcode($attributes, $content) {
            extract($attributes);

            if (isset($title)) {
                $class = (isset($class)) ? $class : 'sp-default';
                $current = (isset($show) and $show == true) ? ' current' : '';
                $html = '<div class="spoiler-head '.$class.$current.'">'.$title.'</div>';
                $html.= '<div class="spoiler-body '.$class.$current.'">'.Filter::apply('content', $content).'</div>';
                return $html;
            }
        }
        
        public static function _header() {
            if (isset(Plugin::$plugins['markitup'])) {
                echo "<script>$(document).ready(function(){mySettings.markupSet.push({name:'Spoiler', replaceWith:'{spoiler title=\"".__('Title', 'spoiler')."\"}".__('Hidden text', 'spoiler')."{/spoiler}', className:'spoiler-button' });});</script>";                
                echo "<style>.spoiler-button a {background-image:url(".Site::url()."plugins/spoiler/lib/button.png);}</style>";
            }
        }
    }