# Spoiler plugin for Monstra CMS

Links: [http://monstra.promo360.ru/plugin/spoiler](http://monstra.promo360.ru/plugin/spoiler)

## Usage
### Default spoiler
{spoiler title="My title"}text{/spoiler}

![](http://f6.s.qip.ru/2Q3rL7rN.jpg)
### Gray spoiler
{spoiler title="My title" class="sp-gray"}text{/spoiler}

![](http://f6.s.qip.ru/2Q3rL7rP.jpg)
### Black spoiler
{spoiler title="My title" class="sp-black"}text{/spoiler}

![](http://f5.s.qip.ru/2Q3rL7rQ.jpg) 
### Green spoiler
{spoiler title="My title" class="sp-green"}text{/spoiler}

![](http://f6.s.qip.ru/2Q3rL7rR.jpg) 
### Red spoiler
{spoiler title="My title" class="sp-red"}text{/spoiler}

![](http://f5.s.qip.ru/2Q3rL7rS.jpg) 

## Installation
* Download the latest release 
* Extract the content of the zip file into the Monstra 'plugins' directory

## History
* v.1.0.0 - first public release
* v.1.0.1 - localization fixed, stylesheet bug fixed
* v.1.0.2 - javascript bug fixed
* v.1.0.3 - added Filter::apply
* v.1.0.4 - added MarkitUp button

## Requirements
* Monstra CMS version 2.2.x or 2.3.x

Copyright (c) 2012-2014 Yudin Evgeniy / JINN [info@promo360.ru]