<?php

    return array(
        'spoiler' => array(
            'Spoiler' => 'Spoiler',
            'Spoiler plugin for Monstra' => 'Show/hide text',
            'Title' => 'Title',
            'Hidden text' => 'Hidden text',
        )
    ); 