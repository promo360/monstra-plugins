<?php

    return array(
        'spoiler' => array(
            'Spoiler' => 'Спойлер',
            'Spoiler plugin for Monstra' => 'Скрыть/показать текст',
            'Title' => 'Заголовок',
            'Hidden text' => 'Скрытый текст',
        )
    ); 